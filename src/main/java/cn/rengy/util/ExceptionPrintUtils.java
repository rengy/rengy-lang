package cn.rengy.util;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ExceptionPrintUtils {
	
	public static String getTrace(Throwable t) {
		StringWriter stackTrace = new StringWriter();
		t.printStackTrace(new PrintWriter(stackTrace));
		stackTrace.flush();
		return stackTrace.toString();
	}
}
