package cn.rengy.lang;

/**
 * 自定义异常
 * @author rengy
 *
 */
public class CustomException  extends RuntimeException implements ErrorCode{

	private static final long serialVersionUID = 7865582990348117009L;
	private Object errorCode=1;
	
	@Override
	public Object getErrorCode() {
		return errorCode;
	}
	public CustomException() {
        super();
    }
	public CustomException(Object errorCode, String errorMsg){
		super(errorMsg);
		this.errorCode=errorCode;
		
	}
    public CustomException(String message) {
        super(message);
    }

    public CustomException(String message, Throwable cause) {
        super(message, cause);
    }

    public CustomException(Throwable cause) {
        super(cause);
    }
}
