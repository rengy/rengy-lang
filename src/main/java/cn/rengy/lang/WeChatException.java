package cn.rengy.lang;


/**
 * 异常处理
 */
public class WeChatException extends CustomException {

	private static final long serialVersionUID = 1L;
	
	public WeChatException() {
        super();
    }
	public WeChatException(Object errorCode, String errorMsg){
		super(errorCode,errorMsg);
	}
    public WeChatException(String message) {
        super(message);
    }

    public WeChatException(String message, Throwable cause) {
        super(message, cause);
    }

    public WeChatException(Throwable cause) {
        super(cause);
    }
}
