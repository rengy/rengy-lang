package cn.rengy.lang;

import java.util.List;
import java.util.Map;

public interface ObjectTranscoder {


	void transcode(String transCodeKey,List<Map<String,Object>> list);
	void transcode(String transCodeKey,Map<String,Object> map);
	


}
