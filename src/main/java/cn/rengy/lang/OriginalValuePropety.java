package cn.rengy.lang;

import java.lang.annotation.*;

/**
 * 原始值注解
 * 当属性值为java关键字或保留字时使用
 * 通常在第三方api中用到
 */
@Target({ElementType.FIELD })//作用目标 字段
@Retention(RetentionPolicy.RUNTIME)//运行时保留
@Documented//包含在javadoc中
public @interface OriginalValuePropety{
	
	String value() default "";
}
