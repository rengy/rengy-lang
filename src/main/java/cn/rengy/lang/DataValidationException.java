package cn.rengy.lang;

/**
  *  数据校验未通过
 * @author rengy
 *
 */
public class DataValidationException extends CustomException {

	private static final long serialVersionUID = 6627273545747762215L;
	
	public DataValidationException() {
        super();
    }
	public DataValidationException(Object errorCode, String errorMsg){
		super(errorCode,errorMsg);
	}
    public DataValidationException(String message) {
        super(message);
    }

    public DataValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataValidationException(Throwable cause) {
        super(cause);
    }
}
