package cn.rengy.lang;

import java.util.concurrent.Callable;

public interface CacheService {

    void removeSqlCache(String sqlType,String cacheType,String cache_operate,String cache_name,String parameterType,Object parameter,String cache_key_param,Integer cache_key_index);
    <T> T getLocalCache(String cacheName, String key, Callable<T> valueLoader);


}
