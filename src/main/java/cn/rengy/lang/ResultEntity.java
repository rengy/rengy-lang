package cn.rengy.lang;



import java.io.Serializable;
/**
 * 通用结果返回
 * @author rengy
 * 
 * @param <T>
 */
public class ResultEntity<C,T> implements Serializable {

	private static final long serialVersionUID = 1L;
	protected Boolean success;
	protected C code;
	protected String message;
	protected T data;
	/**
	 * 消耗时间
	 */
	protected Integer spendTime;
	public ResultEntity() {
		
	}
	public ResultEntity(Boolean success,C code, String message, T data) {
		this.success = success;
		this.code = code;
		this.message = message;
		this.data = data;
	}
	
	public static <C,T> ResultEntity<C,T> ok() {
		return new ResultEntity<C,T>(Boolean.TRUE,null, "success", null);
	}
	public static <C,T> ResultEntity<C,T> code(C code) {
		return new ResultEntity<C,T>(Boolean.TRUE,code, "success", null);
	}
	public static <C,T> ResultEntity<C,T> ok(T body) {
		if(body instanceof ResultEntity){
			throw new RuntimeException("ResultEntity的data不能设置为ResultEntity");
		}
		return new ResultEntity<C,T>(Boolean.TRUE,null, "success", body);
	}
	public static <C,T> ResultEntity<C,T> ok(C code,T body) {
		if(body instanceof ResultEntity){
			throw new RuntimeException("ResultEntity的data不能设置为ResultEntity");
		}
		return new ResultEntity<C,T>(Boolean.TRUE,code, "success", body);
	}
	public static <C,T> ResultEntity<C,T> fail() {
		return new ResultEntity<C,T>(Boolean.FALSE,null, "fail", null);
	}
	public static <C,T> ResultEntity<C,T> fail(String message) {
		return new ResultEntity<C,T>(Boolean.FALSE,null, message, null);
		
	}
	
	public static <C,T> ResultEntity<C,T> fail(C code,String message) {
		return new ResultEntity<C,T>(Boolean.FALSE,code, message, null);
		
	}
	
	public Boolean isSuccess() {
		return success;
	}
	
	public ResultEntity<C,T> setFail() {
		this.success = Boolean.FALSE;
		return this;
	}
	
	public ResultEntity<C,T> setOk() {
		this.success = Boolean.TRUE;
		return this;
	}
	public ResultEntity<C,T> setSuccess(Boolean success) {
		this.success = success;
		return this;
	}


	public C getCode() {
		return code;
	}


	public ResultEntity<C,T> setCode(C code) {
		this.code = code;
		return this;
	}


	public String getMessage() {
		return message;
	}


	public ResultEntity<C,T> setMessage(String message) {
		this.message = message;
		return this;
	}


	public T getData() {
		return data;
	}


	public ResultEntity<C,T> setData(T data) {
		this.data = data;
		return this;
	}

}
