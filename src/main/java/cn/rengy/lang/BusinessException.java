package cn.rengy.lang;

/**
 *  业务异常类
 * @author rengy
 *
 */
public class BusinessException extends CustomException {

	private static final long serialVersionUID = 9190366254756272411L;
	public BusinessException() {
        super();
    }
	public BusinessException(Object errorCode, String errorMsg){
		super(errorCode,errorMsg);
	}
    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }

    public BusinessException(Throwable cause) {
        super(cause);
    }
    
}
