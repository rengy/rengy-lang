package cn.rengy.lang;

import java.io.InputStream;
import java.util.Map;

public interface DataConverter {


	
	String beanToJson(Object bean) ;
	String mapToJson(Map map) ;
	String toJson(Object object) ;
	
	<T> T jsonToBean(String jsonStr, Class<T> valueType)  ;

	Map jsonToMap(String jsonStr);

	String beanToXml(Object bean)  ;

	<T> T xmlToBean(String xml, Class<T> valueType)  ;
	
	<T> T xmlToBean(InputStream inputStream, Class<T> valueType)  ;
	
	Map<String,Object> xmlToMap(String xml)  ;

	Object jsonToObject(String json)  ;
}
