package cn.rengy.lang;

/**
 * 包含错误码
 * @author rengy
 *
 */
public interface ErrorCode {
	
	Object getErrorCode();
	
}
