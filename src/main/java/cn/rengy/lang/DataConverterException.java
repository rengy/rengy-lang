package cn.rengy.lang;

/**
 *  数据转换常类
 * @author rengy
 *
 */
public class DataConverterException extends CustomException {

	private static final long serialVersionUID = 9190366254756272411L;
	public DataConverterException() {
        super();
    }
	public DataConverterException(Object errorCode, String errorMsg){
		super(errorCode,errorMsg);
	}
    public DataConverterException(String message) {
        super(message);
    }

    public DataConverterException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataConverterException(Throwable cause) {
        super(cause);
    }
    
}
